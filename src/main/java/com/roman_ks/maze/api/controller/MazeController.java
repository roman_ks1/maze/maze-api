package com.roman_ks.maze.api.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MazeController {

    @GetMapping("/healthcheck")
    public String healthCheck(){
        return "OK";
    }

}
